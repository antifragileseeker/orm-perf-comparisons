## TODOs/IDEAs:
        * -- Script running the locust with different user numbers, for an amount of time and dumping the data to `locustData/*` folder
        * -- Add examples from other languages and frameworks, (For ex: clojure, Haskell etc..)
        * -- Add test cases for more  realistic/complex workflow like actually doing db operations,
          a sequence of requests like (GET-POST-DELETE) etc..
        * -- Figure out how to measure power consumption and add support/locust extension for it so
          that we can also measure how much power is consumed by each language/framework.
