# -*- coding: utf-8 -*-
# -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.#

#* File Name : test1.py
#
#* Purpose :
#
#* Creation Date : 26-12-2024
#
#* Last Modified : Thu 26 Dec 2024 11:55:41 PM IST
#
#* Created By : Yaay Nands
#_._._._._._._._._._._._._._._._._._._._._.#
from pprint import pprint as pp
import requests
import time
import random
from concurrent.futures import ThreadPoolExecutor
import threading

class WebServerPerformanceTest:
    def __init__(self, base_url, max_threads=10, test_duration=60):
        self.base_url = base_url
        self.max_threads = max_threads  # Maximum concurrent threads
        self.test_duration = test_duration  # Total duration of the test in seconds

    def send_request(self, method, url, data=None):
        """
        Function to send an HTTP request. You can modify this to send POST, PUT, or DELETE.
        """
        try:
            if method == 'GET':
                response = requests.get(url)
            elif method == 'POST':
                response = requests.post(url, data=data)
            elif method == 'PUT':
                response = requests.put(url, data=data)
            elif method == 'DELETE':
                response = requests.delete(url)
            else:
                raise ValueError("Unsupported method.")
            
            print(f"Response status for {method} {url}: {response.status_code}")
            return response.status_code
        except requests.RequestException as e:
            print(f"Error for {method} {url}: {e}")
            return None

    def run_constant_load(self, num_requests):
        """
        Simulate constant request load: A fixed number of requests at a time.
        """
        print(f"Running constant load test with {num_requests} requests.")
        with ThreadPoolExecutor(max_workers=self.max_threads) as executor:
            futures = [executor.submit(self.send_request, 'GET', self.base_url) for _ in range(num_requests)]
            for future in futures:
                future.result()  # Block until all are finished

    def run_ramp_up(self, max_requests):
        """
        Simulate ramp-up load: Gradually increase the number of requests.
        """
        print(f"Running ramp-up test with up to {max_requests} requests.")
        for i in range(1, max_requests + 1):
            print(f"Running {i} requests.")
            self.run_constant_load(i)
            time.sleep(1)  # Optional: Add sleep to simulate time between different load stages

    def run_peak_load(self, max_requests, duration):
        """
        Simulate peak load: Simulate the server handling a heavy burst of traffic.
        """
        print(f"Running peak load test with {max_requests} requests for {duration} seconds.")
        end_time = time.time() + duration
        while time.time() < end_time:
            self.run_constant_load(max_requests)
            time.sleep(1)

    def run_varying_load(self, num_patterns):
        """
        Simulate varying loads over time. Each pattern can have different types of requests.
        """
        for _ in range(num_patterns):
            # Choose random request type and pattern
            method = random.choice(['GET', 'POST', 'PUT', 'DELETE'])
            load = random.randint(5, 20)  # Random number of requests
            self.run_constant_load(load)
            time.sleep(random.uniform(0.5, 2))  # Add some random delay between patterns

    def start_test(self):
        """
        This function can trigger different types of load patterns.
        """
        print(f"Starting test for {self.test_duration} seconds.")
        start_time = time.time()
        while time.time() - start_time < self.test_duration:
            pattern_type = random.choice(['constant', 'ramp_up', 'peak', 'varying'])
            if pattern_type == 'constant':
                self.run_constant_load(10)
            elif pattern_type == 'ramp_up':
                self.run_ramp_up(20)
            elif pattern_type == 'peak':
                self.run_peak_load(50, 10)
            else:
                self.run_varying_load(5)

            time.sleep(1)  # Optional: Give a small delay between different load patterns

    def execute(self):
        """
        Executes the test and provides a summary.
        """
        try:
            self.start_test()
            print("Test completed.")
        except KeyboardInterrupt:
            print("Test interrupted.")
        except Exception as e:
            print(f"Error during test execution: {e}")

if __name__ == "__main__":
    # Initialize test parameters
    test = WebServerPerformanceTest(base_url="http://127.0.0.1", max_threads=10, test_duration=60)
    
    # Start performance testing
    test.execute()

