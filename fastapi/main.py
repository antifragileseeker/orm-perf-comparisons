# -*- coding: utf-8 -*-
# -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.#

#* File Name : fastapi_main.py
#
#* Purpose :
#
#* Creation Date : 17-07-2020
#
#* Last Modified : Monday 19 October 2020 12:52:55 PM IST
#
#* Created By :

#_._._._._._._._._._._._._._._._._._._._._.#
from typing import Optional

from fastapi import FastAPI

app = FastAPI()


@app.get("/hello")
def read_root():
    return "Hello World"


@app.get("/hello/{name}")
def read_item(name: str, q: Optional[str] = None):
    return "Hello {}".format(name)
