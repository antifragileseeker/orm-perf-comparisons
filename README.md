## What:
* -- a simple set of example scripts for testing high load traffic on orm libraries.
* -- based on locust examples from [here](https://locust.io/)
* --
## Why:
* -- I wanted to compare [actix](https://actix.rs/), [rocket](https://rocket.rs/), [fastapi](https://fastapi.tiangolo.com/) and [elixir](https://elixir-lang.org/getting-started/introduction.html)

## How:
* -- To run the locust tests first switch to the repo dir, activate virtualenv which has locust
  installed.
* -- Then run `locust` command
* -- Visit the link url [http://127.0.0.1:8089](http://127.0.0.1:8089)
* -- Specify the no. of users to be tested, http://hostname:port for the webservice to be test and
  start swarming.
