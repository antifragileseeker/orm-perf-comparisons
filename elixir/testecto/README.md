# Testecto

**TODO: Add description**

## Installation

  1. Add testecto to your list of dependencies in mix.exs:

        def deps do
          [{:testecto, "~> 0.0.1"}]
        end

  2. Ensure testecto is started before your application:

        def application do
          [applications: [:testecto]]
        end
