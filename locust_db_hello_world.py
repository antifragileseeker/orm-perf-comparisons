# -*- coding: utf-8 -*-
# -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.#

#* File Name : locust_db_hello_world.py
#
#* Purpose :
#
#* Creation Date : 13-10-2020
#
#* Last Modified : Wednesday 14 October 2020 09:21:51 AM IST
#
#* Created By : Nandhini Anand Jeyahar

#_._._._._._._._._._._._._._._._._._._._._.#

from locust import HttpLocust, TaskSet, task, constant


class HelloWorldTask(TaskSet):
    @task
    def about(self):
        self.client.get("/hellodb")


class WebsiteUser(HttpLocust):
    task_set = HelloWorldTask
    wait_time = constant(0.6)
