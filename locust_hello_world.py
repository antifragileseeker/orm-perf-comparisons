# -*- coding: utf-8 -*-
# -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.#

#* File Name : locust_hello_world.py
#
#* Purpose :
#
#* Creation Date : 13-10-2020
#
#* Last Modified : Sunday 18 October 2020 10:35:56 PM IST
#
#* Created By :

#_._._._._._._._._._._._._._._._._._._._._.#
from locust import HttpLocust, TaskSet, task, constant


class HelloWorldTask(TaskSet):
    @task
    def helloWorld(self):
        self.client.get("/hello")

    @task
    def helloName(self):
        self.client.get("/hello/Nandhini")


class WebsiteUser(HttpLocust):
    task_set = HelloWorldTask
    wait_time = constant(0.6)
