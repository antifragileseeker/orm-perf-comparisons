# -*- coding: utf-8 -*-
# -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.#

#* File Name : test2.py
#
#* Purpose :
#
#* Creation Date : 26-12-2024
#
#* Last Modified : Thu 26 Dec 2024 11:57:18 PM IST
#
#* Created By : Yaay Nands
#_._._._._._._._._._._._._._._._._._._._._.#
from pprint import pprint as pp
import requests
import time
import random
import logging
import threading
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from datetime import datetime

class WebServerPerformanceTest:
    def __init__(self, base_url, max_threads=10, test_duration=60, distributed=False, num_processes=1):
        self.base_url = base_url
        self.max_threads = max_threads  # Maximum concurrent threads per process
        self.test_duration = test_duration  # Total duration of the test in seconds
        self.distributed = distributed  # Whether to use distributed testing
        self.num_processes = num_processes  # Number of processes for distributed testing
        
        # Metrics
        self.success_count = 0
        self.failure_count = 0
        self.total_response_time = 0
        self.total_requests = 0
        
        # Set up logging
        self.setup_logging()

    def setup_logging(self):
        """Sets up logging configuration."""
        logging.basicConfig(
            filename=f"logs/performance_test_{datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.log",
            level=logging.INFO,
            format="%(asctime)s - %(levelname)s - %(message)s"
        )
        logging.info("Starting performance test.")

    def send_request(self, method, url, data=None):
        """
        Function to send an HTTP request. You can modify this to send POST, PUT, or DELETE.
        Also tracks metrics and logs request details.
        """
        try:
            start_time = time.time()
            
            if method == 'GET':
                response = requests.get(url)
            elif method == 'POST':
                response = requests.post(url, data=data)
            elif method == 'PUT':
                response = requests.put(url, data=data)
            elif method == 'DELETE':
                response = requests.delete(url)
            else:
                raise ValueError("Unsupported method.")
            
            response_time = time.time() - start_time
            self.total_response_time += response_time
            self.total_requests += 1
            
            if response.status_code == 200:
                self.success_count += 1
            else:
                self.failure_count += 1
            
            logging.info(f"Request {method} {url} completed with status {response.status_code}. Response time: {response_time:.4f}s.")
            return response.status_code, response_time
        except requests.RequestException as e:
            self.failure_count += 1
            logging.error(f"Error for {method} {url}: {e}")
            return None, None

    def run_constant_load(self, num_requests):
        """
        Simulate constant request load: A fixed number of requests at a time.
        """
        logging.info(f"Running constant load test with {num_requests} requests.")
        with ThreadPoolExecutor(max_workers=self.max_threads) as executor:
            futures = [executor.submit(self.send_request, 'GET', self.base_url) for _ in range(num_requests)]
            for future in futures:
                future.result()  # Block until all are finished

    def run_ramp_up(self, max_requests):
        """
        Simulate ramp-up load: Gradually increase the number of requests.
        """
        logging.info(f"Running ramp-up test with up to {max_requests} requests.")
        for i in range(1, max_requests + 1):
            logging.info(f"Running {i} requests.")
            self.run_constant_load(i)
            time.sleep(1)  # Optional: Add sleep to simulate time between different load stages

    def run_peak_load(self, max_requests, duration):
        """
        Simulate peak load: Simulate the server handling a heavy burst of traffic.
        """
        logging.info(f"Running peak load test with {max_requests} requests for {duration} seconds.")
        end_time = time.time() + duration
        while time.time() < end_time:
            self.run_constant_load(max_requests)
            time.sleep(1)

    def run_varying_load(self, num_patterns):
        """
        Simulate varying loads over time. Each pattern can have different types of requests.
        """
        for _ in range(num_patterns):
            # Choose random request type and pattern
            method = random.choice(['GET', 'POST', 'PUT', 'DELETE'])
            load = random.randint(5, 20)  # Random number of requests
            self.run_constant_load(load)
            time.sleep(random.uniform(0.5, 2))  # Add some random delay between patterns

    def start_test(self):
        """
        This function can trigger different types of load patterns.
        """
        logging.info(f"Starting test for {self.test_duration} seconds.")
        start_time = time.time()
        while time.time() - start_time < self.test_duration:
            pattern_type = random.choice(['constant', 'ramp_up', 'peak', 'varying'])
            if pattern_type == 'constant':
                self.run_constant_load(10)
            elif pattern_type == 'ramp_up':
                self.run_ramp_up(20)
            elif pattern_type == 'peak':
                self.run_peak_load(50, 10)
            else:
                self.run_varying_load(5)

            time.sleep(1)  # Optional: Give a small delay between different load patterns

    def collect_metrics(self):
        """Collect and log the performance metrics after the test."""
        logging.info(f"Test completed. Total requests: {self.total_requests}.")
        logging.info(f"Successful requests: {self.success_count}. Failure requests: {self.failure_count}.")
        if self.total_requests > 0:
            avg_response_time = self.total_response_time / self.total_requests
            logging.info(f"Average response time: {avg_response_time:.4f}s.")
        else:
            logging.warning("No successful requests, unable to calculate average response time.")

    def execute(self):
        """
        Executes the test and provides a summary.
        """
        try:
            if self.distributed:
                self.run_distributed_test()
            else:
                self.start_test()

            self.collect_metrics()
            logging.info("Test completed.")
        except KeyboardInterrupt:
            logging.info("Test interrupted.")
        except Exception as e:
            logging.error(f"Error during test execution: {e}")

    def run_distributed_test(self):
        """
        Run the performance test across multiple processes (distributed testing).
        """
        logging.info("Running distributed test.")
        with ProcessPoolExecutor(max_workers=self.num_processes) as executor:
            futures = [executor.submit(self.start_test) for _ in range(self.num_processes)]
            for future in futures:
                future.result()  # Block until all processes are finished

if __name__ == "__main__":
    # Initialize test parameters
    test = WebServerPerformanceTest(base_url="http://127.0.0.1", max_threads=10, test_duration=60, distributed=True, num_processes=2)
    
    # Start performance testing
    test.execute()

