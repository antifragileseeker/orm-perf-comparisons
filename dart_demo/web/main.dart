import 'dart:io';

Future main() async {
    var server = await HttpServer.bind(
        InternetAddress.loopbackIPv4,
        8000,
        );
    print('Listening on localhost:${server.port}');

    await for (HttpRequest request in server) {
        handleRequest(request);
        request.response.write('Hello World');
        await request.response.close();
        }
    }

// #docregion handleRequest
void handleRequest(HttpRequest request) {
  try {
    // #docregion request-method
    print(request.uri);
    if (request.method == 'GET') {
      handleGet(request);
    } else {
      // #enddocregion handleRequest
      request.response
        ..statusCode = HttpStatus.methodNotAllowed
        ..write('Unsupported request: ${request.method}.')
        ..close();
      // #docregion handleRequest
    }
    // #enddocregion request-method
  } catch (e) {
    print('Exception in handleRequest: $e');
  }
  print('Request handled.');
}

// #docregion handleGet, statusCode, uri, write
void handleGet(HttpRequest request) {

    final response = request.response;
    response.statusCode = HttpStatus.ok;
    print(request.uri.path);
    if (request.uri == '/hello') {
        response..writeln('Hello World');
    } else if (request.uri == '/') {
        response.statusCode = HttpStatus.notImplemented;
        response..writeln('Sorry Unknown URI');
    }
    else {
        var name = request.uri.path.split('/');
        if (name.length > 2) {
            response..writeln('Hello ' +  name[2] + ' World');
        } else {
            response..writeln('Sorry Unknown URI');
        }

    }
}

