# -*- coding: utf-8 -*-
# -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.#

#* File Name : blacksheep_ex.py
#
#* Purpose :
#
#* Creation Date : 26-12-2024
#
#* Last Modified : Thu 26 Dec 2024 08:49:08 PM IST
#
#* Created By : Yaay Nands
#_._._._._._._._._._._._._._._._._._._._._.#

from typing import Optional

from blacksheep import Application, Response, get, text, FromHeader, FromCookie

app = Application()


class FromAcceptHeader(FromHeader[str]):
    name = "Accept"


class FromFooCookie(FromCookie[Optional[str]]):
    name = "foo"


@get("/")
def home(accept: FromAcceptHeader, foo: FromFooCookie) -> Response:
    return text(
        f"""
        Accept: {accept.value}
        Foo: {foo.value}
        """
    )

