#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

#[cfg(test)] mod tests;

#[get("/hello/<name>")]
fn hello(name: String) -> String {
    format!("Hello, {}", name)
}

#[get("/hello")]
fn hi() -> String {
   format!("Hello World")
}

fn main() {
    rocket::ignite().mount("/", routes![hello, hi]).launch();
}
